import { SortBase } from './SortBase';

// Quick sort
export class ASort extends SortBase {
  protected sort(): void {
    this.sortPartition(0, this.sortedValues.length - 1);
  }

  private sortPartition(leftIndex, rightIndex) {
    const partitionIndex = this.getPartitionIndex(leftIndex, rightIndex);
    if (leftIndex < partitionIndex - 1) {
      this.sortPartition(leftIndex, partitionIndex - 1);
    }
    if (rightIndex > partitionIndex) {
      this.sortPartition(partitionIndex, rightIndex);
    }
  }

  private getPartitionIndex(leftIndex: number, rightIndex: number): number {
    const pivotIndex = Math.floor((rightIndex + leftIndex) /  2);
    const pivotElement = this.sortedValues[pivotIndex];
    let left = leftIndex;
    let right = rightIndex;

    while (left <= right) {
      while (this.sortedValues[left] < pivotElement) {
        left += 1;
      }
      while (this.sortedValues[right] > pivotElement) {
        right -= 1;
      }
      if (left <= right) {
        this.swapElements(left, right);
        left += 1;
        right -= 1;
      }
    }
    return left;
  }
}
