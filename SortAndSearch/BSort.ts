import { SortBase } from './SortBase';

// Bubble sort
export class BSort extends SortBase {
  protected sort(): void {
    let nextPassNeeded = false;

    for (let i = 0; i < this.sortedValues.length - 1; i += 1) {
      if (this.sortedValues[i] > this.sortedValues[i + 1]) {
        this.swapElements(i, i + 1);
        nextPassNeeded = true;
      }
    }
    if (nextPassNeeded) {
      this.sort();
    }
  }
}
