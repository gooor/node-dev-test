import { ASort } from './ASort';
import { BSort } from './BSort';
import { BSearch } from './BSearch';

const unsorted = [13, 2, 17, 5, 77, 22, 83, 65, 14, 9, 0, 4, 7, 32];
const elementsToFind = [ 1, 5, 13, 27, 77];

console.log(new ASort(unsorted).sorted);
console.log(new BSort(unsorted).sorted);
elementsToFind.forEach(i => {
  console.log(BSearch.instance.search(elementsToFind, i), BSearch.instance.operations);
});

console.log(BSearch.instance.search(elementsToFind, 35), BSearch.instance.operations);