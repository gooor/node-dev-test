export class BSearch {
  private static _instance: BSearch;
  private operationsCounter: number;

  private constructor() {}

  static get instance(): BSearch {
    if (!this._instance) {
      this._instance = new this();
    }
    return this._instance;
  }

  public search(input: number[], value: number): number {
    this.operationsCounter = 0;
    
    return this.searchInternal(input, value);
  }

  private searchInternal(input: number[], value: number): number {
    this.operationsCounter += 1;
    const middle = Math.floor(input.length / 2);
    if (input.length === 0) {
      return -1;
    }
    if (input[middle] === value) {
      return middle;
    } else if (input[middle] > value) {
      return this.searchInternal(input.slice(0, middle), value);
    } else {
      const index = this.searchInternal(input.slice(middle + 1, input.length), value);
      if (index === -1) {
        return index;
      } else {
        return index + middle + 1;
      }
    }
  }

  get operations(): number {
    return this.operationsCounter;
  }
}