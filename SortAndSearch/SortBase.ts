export abstract class SortBase {
  protected values: number[];
  protected sortedValues: number[];

  constructor(values: number[]) {
    this.values = values;
    this.sortedValues = [...this.values];
    if (this.values.length < 2) {
      return;
    }
    this.sort();
  }

  public get sorted(): number[] {
    return this.sortedValues;
  }

  protected sort(): void {}

  protected swapElements(left: number, right: number): void {
    [this.sortedValues[left], this.sortedValues[right]] = [this.sortedValues[right], this.sortedValues[left]];
  }
}