import { ReportService } from './ReportService';
import { OrderMapper } from '../../Order/Service/OrderMapper';
import { OrderMapped } from 'src/Order/Model/Order';

jest.mock('../../Order/Service/OrderMapper');

const mockedOrders: OrderMapped[] = [{
  number: '1',
  customer: {
    id: 1,
    firstName: 'A',
    lastName: 'B',
  },
  createdAt: 'X',
  products: [{
    id: 1,
    name: 'X',
    price: 2.5,
  }],
}, {
  number: '2',
  customer: {
    id: 1,
    firstName: 'A',
    lastName: 'B',
  },
  createdAt: 'X',
  products: [{
    id: 1,
    name: 'X',
    price: 2.5,
  }, {
    id: 2,
    name: 'Y',
    price: 3.5,
  }],
}, {
  number: '1',
  customer: {
    id: 2,
    firstName: 'D',
    lastName: 'E',
  },
  createdAt: 'X',
  products: [{
    id: 1,
    name: 'X',
    price: 2.5,
  }],
}];

describe('ReportService', () => {
  let service: ReportService;
  beforeEach(() => {
    service = new ReportService();
    service.orderMapper = new OrderMapper();
  });

  describe('bestSellers', () => {
    it('returns best sellers', async () => {
      const ordersSpy = jest.spyOn(service.orderMapper, 'orders').mockResolvedValue(mockedOrders);
      const data = await service.bestSellers('X');
      expect(data).toEqual([{
        productName: 'X',
        quantity: 3,
        totalPrice: 7.5,
      }, {
        productName: 'Y',
        quantity: 1,
        totalPrice: 3.5,
      }]);
      expect(ordersSpy).toBeCalledWith('X');
    });
  });

  describe('bestBuyers', () => {
    it('returns best buyers', async () => {
      const ordersSpy = jest.spyOn(service.orderMapper, 'orders').mockResolvedValue(mockedOrders);
      const data = await service.bestBuyers('X');
      expect(data).toEqual([{
        customerName: 'A B',
        totalPrice: 8.5,
      }, {
        customerName: 'D E',
        totalPrice: 2.5,
      }]);
      expect(ordersSpy).toBeCalledWith('X');
    });
  });
});