import { Injectable, Inject } from '@nestjs/common';
import { OrderMapper } from '../../Order/Service/OrderMapper';
import { IBestSellers, IBestBuyers } from '../Model/IReports';
import { Product } from '../../Order/Model/Product';
import { OrderMapped } from '../../Order/Model/Order';

@Injectable()
export class ReportService {
  @Inject() orderMapper: OrderMapper;

  public async bestSellers(date: string): Promise<IBestSellers[]> {
    const orders = await this.orderMapper.orders(date);
    const flatted = orders
      .reduce<Product[]>((out, order: OrderMapped) => [
        ...out,
        ...order.products,
      ], []);
      
    return Object.values(flatted.reduce<{[id: string]: IBestSellers}>((map, product: Product) => {
      map[product.id] = map[product.id] || {
        productName: product.name,
        quantity: 0,
        totalPrice: 0,
      };
      map[product.id].quantity += 1;
      map[product.id].totalPrice += product.price;
      return map;
    }, {})).sort((a, b) => b.totalPrice - a.totalPrice);
  }

  public async bestBuyers(date: string): Promise<IBestBuyers[]> {
    const orders = await this.orderMapper.orders(date);
    const map = orders.reduce<{[id: string]: IBestBuyers}>((out, order: OrderMapped) => {
      if (!out[order.customer.id]) {
        out[order.customer.id] = {
          customerName: `${order.customer.firstName} ${order.customer.lastName}`,
          totalPrice: 0,
        };
      }
      out[order.customer.id].totalPrice += order.products.reduce((total, i) => total + i.price, 0);
      return out;
    }, {});
    return Object.values(map).sort((a, b) => b.totalPrice - a.totalPrice);
  }
}