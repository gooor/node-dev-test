import { Controller, Get, Param, Inject } from '@nestjs/common';
import { IBestSellers, IBestBuyers } from '../Model/IReports';
import { ReportService } from '../Service/ReportService';

@Controller()
export class ReportController {
  @Inject() reportService: ReportService;

  @Get("/report/products/:date")
  async bestSellers(@Param("date") date: string): Promise<IBestSellers[]> {
    return await this.reportService.bestSellers(date);
  }
  
  @Get("/report/customer/:date")
  async bestBuyers(@Param("date") date: string): Promise<IBestBuyers[]> {
    return await this.reportService.bestBuyers(date);
  }
}
