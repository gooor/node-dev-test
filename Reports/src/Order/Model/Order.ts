import { Customer } from './Customer';
import { Product } from './Product';

export class Order {
  number: string;
  customer: number;
  createdAt: string;
  products: number[];
}

export interface OrderMapped {
  number: string;
  createdAt: string;
  customer: Customer;
  products: Product[];
}