import { Injectable, Inject } from '@nestjs/common';
import { Repository } from './Repository';
import { OrderMapped, Order } from '../Model/Order';

@Injectable()
export class OrderMapper {
  @Inject() repository: Repository;

  public async orders(date: string): Promise<OrderMapped[]> {
    const orders = await this.repository.fetchOrders();
    const products = await this.repository.fetchProducts();
    const customers = await this.repository.fetchCustomers();
    return orders
      .filter(order => order.createdAt === date)
      .map((order: Order) => ({
        ...order,
        customer: customers.find(i => i.id === order.customer),
        products: order.products.map(id => products.find(i => i.id === id)),
      }));
  }
}
