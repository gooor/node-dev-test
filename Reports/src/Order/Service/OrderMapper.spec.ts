import { OrderMapper } from './OrderMapper';
import { Repository } from './Repository';

jest.mock('./Repository');

const mockedOrders = [{
  number: '1',
  customer: 1,
  createdAt: 'X',
  products: [1],
}, {
  number: '2',
  customer: 2,
  createdAt: 'Y',
  products: [1, 2],
}];
const mockedCustomers = [{
  id: 1, firstName: '', lastName: '',
}, {
  id: 2, firstName: '', lastName: '',
}];

const mockedProducts = [{
  id: 1, name: '', price: 2,
}, {
  id: 2, name: '', price: 3,
}]

describe('OrderMapper', () => {
  let orderMapper: OrderMapper;

  beforeEach(() => {
    orderMapper = new OrderMapper();
    orderMapper.repository = new Repository();
  });

  describe('date out of range', () => {
    it('returns filterd out orders for given date', async () => {
      expect.assertions(4);
  
      const ordersSpy = jest.spyOn(orderMapper.repository, 'fetchOrders').mockResolvedValue(mockedOrders);
      const productsSpy = jest.spyOn(orderMapper.repository, 'fetchProducts').mockResolvedValue(mockedProducts);
      const customersSpy = jest.spyOn(orderMapper.repository, 'fetchCustomers').mockResolvedValue(mockedCustomers);
      const orders = await orderMapper.orders('');
      expect(orders).toEqual([]);
      expect(ordersSpy).toBeCalled();
      expect(productsSpy).toBeCalled();
      expect(customersSpy).toBeCalled();
    });
  });

  describe('date in range', () => {
    it('returns filterd out orders for given date', async () => {
      expect.assertions(4);
  
      const ordersSpy = jest.spyOn(orderMapper.repository, 'fetchOrders').mockResolvedValue(mockedOrders);
      const productsSpy = jest.spyOn(orderMapper.repository, 'fetchProducts').mockResolvedValue(mockedProducts);
      const customersSpy = jest.spyOn(orderMapper.repository, 'fetchCustomers').mockResolvedValue(mockedCustomers);
      const orders = await orderMapper.orders('X');
      expect(orders).toEqual([{ number: '1', createdAt: 'X', customer: {id: 1, firstName: '', lastName: ''}, products: [{id: 1, name: '', price: 2}]}]);
      expect(ordersSpy).toBeCalled();
      expect(productsSpy).toBeCalled();
      expect(customersSpy).toBeCalled();
    });
  });
});
